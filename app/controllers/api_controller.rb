class ApiController < ApplicationController
  before_action :require_login, except: [:index, :login_via_access_token]

  def index
    if !user_signed_in?
      render text: 'Please sign up to view this page'
      return 
    end
    @access_keys_list = current_user.access_tokens.order(created_at: :DESC)
  end

  def get_access_key
    if !AccessToken.get_access_key( current_user.id )
      AccessToken.new( user_id: current_user.id ).save
    end
    render json: {access_key: AccessToken.get_access_key(current_user.id), success: true}
  
  end

  # private

  def check_access_key
    result = params[:access_token] == AccessToken.get_access_key(current_user.id)
    render json: result
  end

  def access_keys_list
    @access_keys = current_user.access_tokens.order(created_at: :DESC)
    render partial: "api/access_keys_list.html.haml", access_keys_list: @access_keys
  end

  def disable_access_key
    @access_key = AccessToken.find(params[:id])
    needle = @access_key.active ? "was active" : "no need to deactivate"
    if @access_key
      @access_key.disable
    end

    response = {
      message: "Disable access_key #{@access_key.explain}, #{needle}",
      success: !@access_key.active
    }

    respond_to do |format|
      format.html { redirect_to :back }
      format.json { render json: response }
    end
  end

  def get_current_account_info
    render json: current_user.pretty
  end

  def remove_inactive_access_tokens
    @to_remove = 0
    if params[:sure]
      @to_remove = current_user.remove_inactive_access_tokens
    end

    render json: {
      removed: @to_remove,
      success: !params[:sure].nil?
    }
  end

  def login_via_access_token
    if user_signed_in?
      render json: { 
        error: 'You are currenly signed in. Sign out or follow your Account', 
        success: false 
      }

      return
    end

    try_access_token_login    

    render json: {
      message: 'Sign in via access_token',
      success: user_signed_in?
    }

  end

  def delete_access_key
    @access_key = AccessToken.find(params[:id])
    if @access_key
      @access_key.destroy
    end

    redirect_to :back
  end

  def require_login
    try_access_token_login
    if !user_signed_in?
      render json: { 
        error: 'You are not signed in to get API Access Key. Please follow link http://vkrushelnytskiy.com/sign_up to register new account', 
        success: false 
      }
    end
  end

  def try_access_token_login
    access_key = AccessToken.find_by_explain(params[:access_token])
    if access_key && access_key.active && access_key.expire > DateTime.now
       sign_in(
         :user, User.find(access_key.user.id)
       )

    elsif params[:access_token] && !user_signed_in?
      render json: { 
        error: 'The access_token is invalid. Please login in to your account, first and generate new access_key', 
        success: false 
      }
      return
    end
  end

end