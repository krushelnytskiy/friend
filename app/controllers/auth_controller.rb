class AuthController < DeviseController
  before_action :restrict_login 


  def auth
  end

  def new_user
  end

  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def restrict_login
    if user_signed_in?
      redirect_to access_index_path
    end
  end

end
