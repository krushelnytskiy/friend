class AccessController < ApplicationController
  def index
  end

  def news
    @news = News.where(published: :true).order(created_at: :DESC).paginate(page: params[:page], per_page: 12)
  end

  def settings
    @types = SettingType.where(enabled: true).map { |i| [i.name, i.id] }
    @my_settings = SettingType.where(enabled: true)
    if params[:slug] 
      @current_setting = SettingType.where(slug: params[:slug])
      if @current_setting.empty?
        redirect_to access_settings_path
      end
    end

  end

  def read_news_article
    @article = News.where(slug: params[:slug]).last
  end

  def enable_setting

  end

  def help
  end

end