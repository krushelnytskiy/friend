class ConstructorController < ApplicationController
  before_action :require_login 

  def app
    @app = App.exists?(slug: params[:slug]) && !params[:slug].nil? ? App.find_by_slug(params[:slug]) : App.new
   
      @url = @app.name.nil? ? constructor_create_app_path : constructor_update_app_path
    
  end

  def new_object
    @object = ApiObject.new
  end

  def create_app
    @app = App.create(app_params)
    @app.save

    redirect_to "/api/app/#{@app.slug}"
  end

  def update_app
  end

  def view_app
    begin
      @app = App.find_by_slug(params[:slug])  
    rescue
      @app = App.new
    end
  end

  private
  
  def require_login
    if !user_signed_in?
      redirect_to root_path
    end
  end

  def app_params
    params.require(:app).permit(:name, :description)
  end

end
