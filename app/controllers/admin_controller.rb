class AdminController < ApplicationController
  before_action :restrict_non_admin


  def index
  end

  def add_to_news
    @article = params[:aid] ? News.find(params[:aid]) : News.new
    render partial: "admin/add_to_news.html.haml", article: @article
  end

  def show_users
    @users = User.all.order(created_at: :DESC).map { |u| u.pretty }
    respond_to do |format|
     format.json { render json: @users }
     format.html { render partial: "admin/show_users.html.haml", users: @users }
    end
  end

  def publish_article
    @article = News.create(news_params)
    @article.short = ActionView::Base.full_sanitizer.sanitize(@article.fulltext)[0..600]

    @article_slug = @article.title.parameterize
    @news_with_same_slug = News.where( slug: @article_slug ).count

    if @news_with_same_slug > 0 
      @article_slug = "#{@article_slug}-#{@article.id}"
    end

    @article.slug = @article_slug

    @article.short = ActionView::Base.full_sanitizer.sanitize(@article.fulltext)[0..600]
    @article.save
    redirect_to :back 
  end

  def delete_article
    @article = News.find(params[:id])
    @article.delete
    redirect_to :back
  end

  private 

  def restrict_non_admin
    if !user_signed_in? || !current_user.admin?
      not_found
    end
  end


  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end

  private

  def news_params
    params.require(:news).permit(:title, :fulltext, :published, :user_id)
  end

end