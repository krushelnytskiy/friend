class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :settings, class_name: 'Settings', foreign_key: 'user_id'
  has_many :access_tokens, class_name: 'AccessToken', foreign_key: 'user_id'
  has_many :news, class_name: 'News', foreign_key: 'user_id'

  def admin?
    self.role && self.role > 2
  end

  def pretty
    user_pretty = {
      id: self.id,
      email: self.email,
      role: self.role,
      access_key: self.access_tokens.where(active: true).map { |a| a.pretty },
      access_keys: self.access_tokens.count
    }
  end

  def to_s
    email
  end

  def remove_inactive_access_tokens
    AccessToken.get_access_key( self.id )
    @inactive_tokens = self.access_tokens.where(active: false)
    @inactive_tokens.each do |token|
      token.destroy
    end

    return @inactive_tokens.map { |e|  e.id }.count
  end
end
