class AccessToken < ActiveRecord::Base
  require 'date'

  belongs_to :user
  before_validation :generate_hash

  def self.check explain, user_id
    self.where(user_id: user_id, explain: explain, active: true).empty? ? false : true
  end

  def self.get_access_key user_id
    access_keys = self.where(user_id: user_id, active: true).order(created_at: :DESC)
    main_access_key = false
    access_keys.each do |access_key|
      if(access_key.expire < DateTime.now)
        access_key.active = false
        access_key.save
      else
        main_access_key = access_key.explain
      end
    end

    return main_access_key
  end

  def pretty
    return {
      access_key: explain,
      created_date: created_at,
      updated_date: updated_at,
      active: active
    }
  end

  def disable
    self.active = false
    self.save
    return self
  end

  private

  def generate_hash
    if self.explain == nil
      self.expire = DateTime.now + 1.day
      self.explain = Digest::SHA1.hexdigest DateTime.now.strftime('%Q').to_s + user.email + user.id.to_s
      self.active = true
    end
  end

end
