class SettingType < ActiveRecord::Base
  before_validation :set_slug

  def to_s
    name
  end

  def enabled?
    self.enabled ? "enabled" : "disabled"
  end

  private

  def set_slug
    self.slug = self.name.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
  end


end
