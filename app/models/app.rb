class App < ActiveRecord::Base
  before_validation :set_slug

  def set_slug
    self.slug = name.parameterize

    if self.find_by_slug(self.slug)
      self.slug += ( '-' + self.id.to_s ) 
    end
  end

  def find_by_slug( p_slug )
    App.where(slug: p_slug).last
  end
end
