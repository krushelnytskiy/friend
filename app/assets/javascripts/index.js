$(document).on("click", "#add_article_button_modal", function() {
  getAddArticleForm(
  function(data) {
    $('#add_article').html(data);
  })
});

$(document).on("click", "#show_users_button_modal", function() {
  getUsersList(
  function(data) {
    $('#show_users').html(data);
  })
});


$(document).on("click", 'button[id$="_button_modal"]', function(e) {
  var modal_select = '.modal#'  + e.currentTarget.id.replace('_button_modal', '');
  console.log(modal_select)
  $(modal_select).show();
});

$(document).on("click", "#close_modal", function() {
  $('.modal').hide();
});

function getAddArticleForm(callback) {
  $.ajax({
    url: 'admin/add_to_news',
    dataType: 'html',

    success: function(data) {
      callback(data);
    }
  })
}

function getUsersList(callback) {
  $.ajax({
    url: 'admin/show_users',
    dataType: 'html',

    success: function(data) {
      callback(data);
    }
  })
}