$(document).on('click', 'button#get_access_key', function() {  
    getCurrentUserAccessKey(function(access_key){
      $('p#access_key').text(access_key)
    });

    getCurrentUserAccessKeysList(function(list) {
      $('p#access_keys_list').html(list);
    });
});

getCurrentUserAccessKeysList(function(list) {
  $('p#access_keys_list').html(list);
});

function getCurrentUserAccessKeysList(callback)
{
  $.ajax({
    url: '/api/access_keys_list',
    data_type: 'html',
    success: function(data) {
      callback(data)
    }
  });
}

function getCurrentUserAccessKey(callback){
  $.ajax({
    url: '/api/get_access_key',
    data_type: 'json',
    success: function(data) {
      callback(data.access_key)
    }
  });
}