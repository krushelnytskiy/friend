Rails.application.routes.draw do
  
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users

  root 'index#index'
  get  'io' => 'index#io'
  get  'sign_in' => 'auth#auth'
  get  'sign_up' => 'auth#new_user'
  
  namespace :access, only: [:index], path: '/u' do
      get 'index', path: '/'
      get 'news'
      get 'help'
      get 'settings'
  end

  get '/u/news/:slug' => 'access#read_news_article'
  get '/u/settings/:slug' => 'access#settings'

  namespace :api, only: [] do
    get 'index', path: '/'
    match 'check_access_key', via: [:get, :post]
    match 'get_access_key', via: [:get, :post]
    match 'access_keys_list', via: [:get, :post]
    match 'disable_access_key', via: [:get, :post], as: :disable_access_key
    match 'delete_access_key', via: [:delete], as: :delete_access_key
    match 'login_via_access_token', via: [:post, :get]
    match 'get_current_account_info', via: [:post, :get]
    match 'remove_inactive_access_tokens', via: [:get, :post]
  end

  namespace :admin, only: [] do
    get 'index', path: '/'
    match 'add_to_news', via: [:get, :post]
    post 'publish_article'
    post 'show_users'
    delete 'delete_article'
  end

  namespace :constructor, path: '/api', only: [] do
    get 'app', path: 'app'
    post 'create_app'
    post 'update_app'
  end

  get 'api/app/:slug/edit' => 'constructor#app'
  match 'api/app/:slug' => 'constructor#view_app', via: [:get, :post]

  # Mercury::Engine.routes
end
