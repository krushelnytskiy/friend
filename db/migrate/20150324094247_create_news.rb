class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :title
      t.boolean :published, default: true
      t.string :short
      t.text :fulltext
      t.integer :user_id
      t.integer :read_count
      t.string :slug

      t.timestamps null: false
    end
  end
end
