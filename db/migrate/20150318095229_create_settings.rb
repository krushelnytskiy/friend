class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.integer :type_id
      t.boolean :pool
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
