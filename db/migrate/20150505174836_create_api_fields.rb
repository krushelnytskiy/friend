class CreateApiFields < ActiveRecord::Migration
  def change
    create_table :api_fields do |t|
      t.integer :object_id
      t.string :name
      t.string :type

      t.timestamps null: false
    end
  end
end
