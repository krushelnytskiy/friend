class CreateApiObjects < ActiveRecord::Migration
  def change
    create_table :api_objects do |t|
      t.boolean :private
      t.integer :app_id
      t.text :description
      t.string :name

      t.timestamps null: false
    end
  end
end
