class CreateApiFieldValues < ActiveRecord::Migration
  def change
    create_table :api_field_values do |t|
      t.integer :field_id
      t.text :value
      t.integer :value_id

      t.timestamps null: false
    end
  end
end
