class CreateSettingTypes < ActiveRecord::Migration
  def change
    create_table :setting_types do |t|
      t.string :name
      t.boolean :enabled

      t.timestamps null: false
    end
  end
end
