class CreateApiValues < ActiveRecord::Migration
  def change
    create_table :api_values do |t|
      t.integer :object_id
      t.text :value

      t.timestamps null: false
    end
  end
end
